// @ts-check

const
  { beforeEach } = require('mocha'),
  process = require('process'),
  path = require('path');

beforeEach(function() {
  process.env.GNUPGHOME = path.join(__dirname, 'data', 'gnupg');
  process.env.PASSWORD_STORE_DIR = path.join(__dirname, 'data', 'store');
  delete process.env.NO_PASS;
});
