// @ts-check
const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),
  { get } = require('lodash'),
  process = require('process'),
  cp = require('child_process'),
  util = require('util'),
  path = require('path'),

  yaml = require('js-yaml');

const execFile = util.promisify(cp.execFile);

describe('oc-gen', function() {
  it('can replace passwords', async function() {
    delete process.env.NO_PASS;
    const ret = await execFile(path.join(__dirname, '..', 'src', 'oc-gen.js'),
      [ path.join(__dirname, 'data', 'openshift-dc.yml'),
        path.join(__dirname, 'data', 'custom-config.yml') ]);

    const json = yaml.load(ret.stdout);

    let config = get(json, [ 'items', 1, 'data', 'config.yml' ]);
    expect(yaml.load(config)).to.have.nested.property('auth.clientSecret',
      'this is my secret');

    config = get(json, [ 'items', 1, 'data', 'custom-config.yml' ]);
    expect(yaml.load(config)).to.have.property('test',
      'this is my secret\nand even more...');
  });
});
