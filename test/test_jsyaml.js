// @ts-check
const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),

  path = require('path'),
  { tryLoadFile } = require('../src');

describe('jsyaml', function() {
  it('can tryLoad a file', function() {
    expect(tryLoadFile('notexist.yml')).to.equal(undefined);
    expect(tryLoadFile(path.join(__dirname, 'data', 'custom-config.yml')))
    .to.deep.equal({ test: "this is my secret\nand even more..." });
  });
});
