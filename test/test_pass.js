// @ts-check
const
  { expect } = require('chai'),
  { describe, it } = require('mocha'),

  process = require('process'),
  { load, dump } = require('../src');

describe('pass', function() {
  it('can replace passwords', function() {
    const ret = load(`
      toto: !pass my/secret
      titi: !pass { line: 1, name: 'my/secret' }`);

    expect(ret).to.deep.equal({
      titi: "this is my secret",
      toto: "this is my secret\nand even more..."
    });
  });

  it('can ignore pass', function() {
    process.env.NO_PASS = '1';

    const ret = load(`
      toto: !pass my/secret
      titi: !pass { line: 1, name: 'my/secret' }`);

    const out = dump(ret);
    expect(out).to.equal(`\
toto: !pass my/secret
titi: !pass 
  line: 1
  name: my/secret
`);
  });
});
