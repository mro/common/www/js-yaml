
import { DumpOptions, LoadOptions, Schema, Type } from 'js-yaml';

export = jsyaml;

export as namespace jsyaml;

declare namespace jsyaml {

  function preload(data: string): string

  function load(data: string, options?: LoadOptions): any

  function dump(data: string, options?: DumpOptions): string

  function tryLoadFile(file: string, options?: LoadOptions): any

  const schema: Schema;

  namespace pass {

    const PasswordType: Type;

    const PasswordMapType: Type;

    class PasswordTagScalar {
      constructor(pass: string, resolve?: boolean)

      get(): string
    }

    class PasswordTagMapping {
      constructor(pass: any, options?: any, resolve?: boolean)

      get(): string
    }
  }
}
