#!/usr/bin/env node
// @ts-check

const { merge } = require('lodash'),
  process = require('process'),
  fs = require('fs'),
  { createCommand } = require('commander'),
  yaml = require('js-yaml'),
  pass = require('./pass');

const schema = yaml.DEFAULT_SCHEMA.extend(
  [ pass.PasswordType, pass.PasswordMapType ]);

/**
 * @param  {string} data
 * @return {string}
 */
function preload(data) {
  return dump(load(data));
}

/**
 * @param  {any} data
 * @param  {any} [options]
 * @return any
 */
function load(data, options) {
  return yaml.load(data, merge({ schema }, options));
}

/**
 * @param  {any} data
 * @param  {any} [options]
 * @return string
 */
function dump(data, options) {
  return yaml.dump(data, merge({ schema }, options));
}

/**
 * @param {string} file
 * @param {any} [options]
 */
function tryLoadFile(file, options) {
  try {
    return load(fs.readFileSync(file).toString(), options);
  }
  catch {
    return undefined;
  }
}

if (require.main === module) {
  const args = createCommand('yaml-prepare')
  .argument('<file>', 'file to preprocess')
  .option('--no-pass', 'do not resolve password tags')
  .parse(process.argv);

  const opts = args.opts();
  process.env.NO_PASS = opts.pass ? '' : '1';

  process.on('unhandledRejection', function(reason) {
    console.warn(reason);
    process.exitCode = 1;
  });

  console.log(preload(fs.readFileSync(args.processedArgs[0]).toString()));
}
else {
  module.exports = { preload, load, dump, schema, pass, tryLoadFile };
}
