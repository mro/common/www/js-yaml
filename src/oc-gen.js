#!/usr/bin/env node
// @ts-check

const fs = require('fs'),
  path = require('path'),
  { preload, load, dump } = require('./index'),
  { createCommand } = require('commander'),
  { find, mapValues, merge, get } = require('lodash');

const opts = createCommand('genconf')
.argument('<deployment>', 'deployment YAML file to modify')
.argument('[config...]', 'configuration files to inject')
.parse(process.argv);

process.on('unhandledRejection', function(reason) {
  console.warn(reason);
  process.exitCode = 1;
});

/** @type {string} */
const outFile = opts.processedArgs[0];
/** @type {string[]} */
const inFiles = opts.processedArgs[1];

/** @type {{ [name: string]: string }} */
const confFiles = {};
inFiles.forEach((conf) => {
  confFiles[path.basename(conf)] = preload(fs.readFileSync(conf).toString());
});

const output = load(fs.readFileSync(outFile).toString());
const config = find(get(output, [ 'items' ]), { kind: 'ConfigMap' });

if (!config) {
  throw new Error('failed to find ConfigMap in: ' + outFile);
}
config.data = mapValues(config.data, (data, name) => {
  if (name.endsWith('.yaml') || name.endsWith('.yml')) {
    return preload(data);
  }
  return data;
});

config.data  = merge(config.data, confFiles);
console.log(dump(output));
