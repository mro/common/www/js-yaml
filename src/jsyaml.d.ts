
declare module "js-yaml" {
  import { SchemaDefinition } from '@types/js-yaml';
  export * from '@types/js-yaml';

  export class Type<T> {
    constructor(tag: string, opts?: TypeConstructorOptions<T>);

    tag: string;

    kind: 'sequence' | 'scalar' | 'mapping' | null;

    resolve(data: any): boolean;

    construct(data: any, type?: string): any;

    instanceOf: typeof T | null;

    predicate: ((data: T) => boolean) | null;

    represent: ((data: T) => any) | { [x: string]: (data: T) => any } | null;

    representName: ((data: T) => any) | null;

    defaultStyle: string | null;

    multi: boolean;

    styleAliases: { [x: string]: any };
  }

  export interface TypeConstructorOptions<T> {
      kind?: 'sequence' | 'scalar' | 'mapping' | undefined;
      resolve?: ((data: any) => boolean) | undefined;
      construct?: ((data: any, type?: string) => any) | undefined;
      instanceOf?: typeof T | undefined;
      predicate?: ((data: T) => boolean) | undefined;
      represent?: ((data: T) => any) | { [x: string]: (data: T) => any } |
        undefined;
      representName?: ((data: T) => any) | undefined;
      defaultStyle?: string | undefined;
      multi?: boolean | undefined;
      styleAliases?: { [x: string]: any } | undefined;
  }

  export class Schema {
    constructor(definition: SchemaDefinition | Type<any>[] | Type<any>);

    extend(types: SchemaDefinition | Type<any>[] | Type<any>): Schema;
  }

  export let DEFAULT_SCHEMA: Schema;
}
