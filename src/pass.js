// @ts-check

const { toString, trim, isNil, get } = require('lodash'),
  { execFileSync } = require('child_process'),
  process = require('process'),
  yaml = require('js-yaml');

class PasswordTagScalar {
  /**
   * @param {string} pass
   * @param {boolean} [resolve=!process.env.NO_PASS]
   */
  constructor(pass, resolve = !process.env.NO_PASS) {
    this.pass = pass;
    this.resolve = resolve;
  }

  get() {
    return trim(toString(execFileSync('pass', [ this.pass ])));
  }
}

class PasswordTagMapping {
  /**
   * @param {any} pass
   * @param {any} [options={}]
   * @param {boolean} [resolve=!process.env.NO_PASS]
   */
  constructor(pass, options = {}, resolve = !process.env.NO_PASS) {
    this.pass = pass;
    this.resolve = resolve;
    this.options = options;
  }

  get() {
    const line = get(this.options, 'line');
    const res = trim(toString(execFileSync('pass', [ this.pass ])));
    if (!isNil(line)) {
      return get(res.split('\n'), [ line - 1 ], '');
    }
    else {
      return res;
    }
  }
}

/** @type {yaml.Type<PasswordTagScalar>} */
const PasswordType = new yaml.Type('!pass', {
  kind: 'scalar',
  /** @param  {PasswordTagScalar} pass */ // @ts-ignore
  represent(pass) {
    return pass.resolve ? pass.get() : pass.pass;
  },
  multi: true,
  /**
   * @this {yaml.Type<PasswordTagScalar>}
   * @param  {PasswordTagScalar} pass
   */
  representName(pass) {
    return pass.resolve ? null : this.tag;
  },
  instanceOf: PasswordTagScalar,
  construct(data) {
    return (!process.env.NO_PASS) ?
      (new PasswordTagScalar(data)).get() :
      (new PasswordTagScalar(data));
  }
});

const PasswordMapType = new yaml.Type('!pass', {
  kind: 'mapping',
  /** @param  {PasswordTagMapping} pass */
  represent(pass) {
    return pass.resolve ? pass.get() : pass.options;
  },
  multi: true,
  /**
   * @this {yaml.Type<PasswordTagMapping>}
   * @param  {PasswordTagMapping} pass
   */
  representName(pass) {
    return pass.resolve ? null : this.tag;
  },
  instanceOf: PasswordTagMapping,
  construct(data) {
    const name = get(data, 'name');
    return (!process.env.NO_PASS) ?
      (new PasswordTagMapping(name, data)).get() :
      (new PasswordTagMapping(name, data));
  }
});

module.exports = {
  PasswordType, PasswordMapType, PasswordTagScalar, PasswordTagMapping
};
