
import { expectType } from 'tsd';

import { load, preload, dump, tryLoadFile } from '.';

expectType<string>(dump(load('test')));

expectType<string>(preload('test'));

tryLoadFile('test.yml')
