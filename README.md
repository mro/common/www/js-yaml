# js-yaml extensions

js-yaml extensions and utilities.

This project brings [pass](https://www.passwordstore.org/) support to js-yaml.

## Usage

```bash
# In your project:
npm install --save-dev git+https://gitlab.cern.ch/mro/common/www/js-yaml.git

# Process a file
yaml-prepare file.yaml
```

### OpenShift configuration files

This tool provides an helper for OpenShift configuration files, it will:
- prepare every _yaml/yml_ entry found in the first _ConfigMap_ found in document ([example](./test/data/openshift-dc.yml#L28)).
- prepare inject any _config files_ provided on the command-line in the _ConfigMap_. ([example](./test/data/custom-config.yml)).

Usage:
```bash
# prepare openshift configuration
oc-gen ./deploy/openshift-dc.yml ./deploy/config*.yml| oc apply -f -
```

## Build

Npm and Node.js are prerequisites to build this library:
```bash
# I'd recommend adding this in your .bashrc
export PATH="./node_modules/.bin:$PATH"

# Install dependencies and tools
npm install
```

### Testing

To run unit-tests (in development mode):
```bash
# Run unit-tests tests
npm test
```
